# atrent's rules

## Vita
1. Se hai avuto una grande idea... l'ha già avuta qualcun altro. Cerca in rete.
1. Se non la trovi... stai sbagliando la ricerca.
1. Se la trovi simile ma la tua è "migliore", cerca ancora o leggi meglio.
1. Non re-inventare la ruota.
1. Se non sai spiegarlo non l'hai capito.
1. Se non sai presentare le tue idee non andrai lontano.
1. Fatti domande scomode.
1. "Assumptions kill" (Reacher).
1. Nella comunicazione vale il "Tutto dovrebbe essere reso il più semplice possibile, ma non più semplice" (attribuita a A.Einstein).
1. Chiedi sempre il permesso, dì "per favore", ringrazia, sorridi, sii gentile.
1. Perdona se puoi, vendicati se vuoi, ma pianifica bene.
1. Nessuno ti deve niente, non devi niente a nessuno.
1. Prezzo di un prodotto e sua qualità sono **lascamente** correlati. (es. Software Libero)

## Tecnologia
1. Il *mouse* è per i disabili: abbiamo (mediamente) dieci dita.
1. POCORGTFO (Proof Of Concept Or Get The Fuck Out)
1. Il software proprietario è "Il Male"™.
1. Il *lock-in* è "Il Male"™.
1. L'obsolescenza programmata è "Il Male"™.
1. L'obsolescenza programmata e il *lock-in* non sono attuabili col Software Libero ("capisci ammè...").
1. Un foglio elettronico non è un database.
1. Non dire mai "l'ho letto su Internet" o "l'ho cercato su Google".
1. Non digitare un URL nel campo "ricerca" di Google. Sai cos'è un URL, vero?
1. WYSIWYG no, WYSIWYM sì. LaTeX sì, Word/Libreoffice no.
1. *Versioning* non è creare tanti file con le date nei nomi: usa *git*.
1. Il tuo hardware può fallire in ogni momento, sii preparato. `Backup != Carneade`
1. Se un sistema/progetto/programma si basa sul *point&click* per funzionare, ha già fallito.
1. *The CLI is your friend.*
1. Ancora oggi vale il "tutto dovrebbe essere un file".
1. Se sai dove abiti devi anche sapere dove hai salvato i tuoi file.
1. C'è sempre un modo sbagliato di usare un tuo prodotto, sarà il primo modo che verrà provato durante una demo.
1. "*Any sufficiently advanced technology is indistinguishable from magic.*" (A.C.Clarke). Per te niente deve essere "magia".
1. Sii pigro: *script*a.
1. Non essere pigro: leggi la documentazione.
1. Se la documentazione non è chiara, leggi il sorgente (è Software Libero, vero?).
1. Un "dataset" non *searchable* non è un dataset.
1. Chiedi sempre i dati e critica pure quelli.
1. Il gergo non è una barriera snobistica, è un (primo) modo per capire se sei dell'ambiente.
1. Quando scrivi una mail sii conciso, completo (**Subject parlante!** Il destinatario deve entrare nel contesto corretto), esplicito, cortese.
1. Le tue mail HTML finiranno in */dev/null*.
1. Se alleghi file in formati proprietari, la tua mail finirà in */dev/null*.
1. I *direct message* (via Telegram/Whatsapp/ecc.) servono solo per le comunicazioni brevi, logistiche, in tempo reale e *expendable*, ogni altra comunicazione va fatta via mail.
1. Se scrivi un testo pensa prima (o solo?) al contenuto e poi alla forma.
1. Un documento "tecnico" deve rispondere alle domande "*Who? What? When? Where? Why?*" (le "five Ws", a cui aggiungere magari anche "*How?*" e "*How much?*")
1. "Non funziona" è una **non informazione**, "funziona sul mio PC" anche.
1. *Sviluppare programmi* è (minimo) 90% **ragionamento** e 10% *coding*.
1. Carta e matita sono (ancora) i migliori strumenti per ragionare.

## Politica
1. Il minarchismo è la via.
1. Oltre il 10% di pressione fiscale le tasse sono un furto.
1. Lo Stato deve essere al servizio del cittadino, non viceversa.
1. Lo Stato deve essere totalmente trasparente, il cittadino totalmente opaco.
1. Tv, giornali e politici mentono, sistematicamente.
1. 'Giornalismo indipendente' è un ossimoro.
1. Se pensi che una iniziativa politica non riguardi te, leggiti [Prima vennero...](http://it.wikipedia.org/wiki/Prima_vennero...)









## links
- ispirato a https://ncis.fandom.com/wiki/Gibbs%27s_Rules
- forse si può prendere cose anche da http://marvin.cs.uidaho.edu/About/ferengi.html